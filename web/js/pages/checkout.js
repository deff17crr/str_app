(function ($) {
    var credit_card_container = $('.credit-card');
    var credit_card_fields = $('input, select', credit_card_container);
    $('.use-old-card select').on('change', function (e) {
        if ($(this).val() == '') {
            credit_card_container.show();
            credit_card_fields.attr('required', true);
        } else {
            credit_card_container.hide();
            credit_card_fields.removeAttr('required');
        }
    });
})(jQuery);