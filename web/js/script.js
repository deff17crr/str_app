(function($) {
    var top_cart = $('.top-cart');
    var cart_items = $('.cart-items', top_cart);
    var loader = $('.loader');
    var cart_items_container = $('.cart-items');
    var add_to_cart = $('.add-to-card');
    var remove = $('.remove');
    var i;

    $('.confirm').on('click', function() {
        return window.confirm("Are you sure?");
    });

    /* event handlers */
    top_cart.on('mouseover', function () {
        cart_items.show();
    });
    top_cart.on('mouseout', function () {
        cart_items.hide();
    });
    top_cart.on('click', function () {
        window.location = Routing.generate('cart_index');
    });
    add_to_cart.on('click', function (e) {
        e.preventDefault();

        var product_id = $(this).data('product-id');
        $.ajax({
            url: Routing.generate('cart_item_add', {'product' : product_id}),
            beforeSend: function () {
                loader.show();
            },
            complete: function (response) {
                if (!response.hasOwnProperty('responseJSON') || response.responseJSON.hasOwnProperty('error')) {
                    return;
                }

                renderTopCart(response.responseJSON);
                
                loader.hide();
            }
        });
    });
    remove.on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var cart_item = $(this).closest('.item').length ? $(this).closest('.item') : $(this).closest('.cart-item');
        var cart_item_id = cart_item.data('cart-item-id');
        $.ajax({
            url: Routing.generate('cart_item_remove', {'cartItem' : cart_item_id}),
            beforeSend: function () {
                loader.show();
            },
            complete: function (response) {
                if (response.responseText == 'ok') {
                    location.reload();
                }

                loader.hide();
            }
        });
    });

    /* render top-right cart items at loading page */
    $.ajax({
        url : Routing.generate('cart_item_list'),
        beforeSend: function () {
            loader.show();
        },
        complete: function (response) {
            if (!response.hasOwnProperty('responseJSON') || response.responseJSON.hasOwnProperty('error')) {
                return;
            }

            renderTopCart(response.responseJSON);
            loader.hide();
        }
    });

    function renderTopCart(items) {
        cart_items_container.html('');
        var total_quantity = 0;
        var total_price = 0;

        for (i in items) {
            var cart_item = items[i];

            var cart_item_element = $('<div class="cart-item" />');

            cart_item_element.attr('data-cart-item-id', cart_item.id);
            cart_item_element.append($('<img class="img-rounded" />').attr('src', cart_item.product_image));
            cart_item_element.append($('<span class="quantity" />').text(' x '+cart_item.quantity));
            cart_item_element.append($('<span class="title" />').text(cart_item.product_title));
            cart_items_container.append(cart_item_element);

            total_quantity += cart_item.quantity;
            total_price += (cart_item.price * cart_item.quantity);
        }

        $('.amount', top_cart).text('$'+total_price.toFixed(2)+' ('+total_quantity+')');
    }
})(jQuery);