<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER  = 'ROLE_USER';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Transaction
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Transaction", mappedBy="user")
     */
    protected $transactions;

    /**
     * @var Order
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order", mappedBy="user")
     */
    protected $orders;

    /**
     * @var CreditCard[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CreditCard", mappedBy="user")
     */
    protected $creditCards;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->transactions = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->creditCards = new ArrayCollection();
    }

    /**
     * @return Transaction
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @return Order
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return CreditCard[]
     */
    public function getCreditCards()
    {
        return $this->creditCards;
    }
}
