<?php
namespace AppBundle\Form;

use AppBundle\Entity\CreditCard;
use AppBundle\Entity\User;
use AppBundle\Repository\CreditCardRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CheckoutType
 * @package AppBundle\Form
 */
class CheckoutType extends AbstractType
{
    /** @var User */
    private $user;

    /**
     * CheckoutType constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser() instanceof User ? $tokenStorage->getToken()->getUser() : null;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('lastName', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('city', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('address', TextType::class, ['constraints' => [new NotBlank()]])
        ;

        $months = array_combine(range(1, 12, 1), range(1, 12, 1));
        $years = array_combine(range(2017, 2027, 1), range(2017, 2027, 1));

        if ($this->user == null) {
            $builder
                ->add('card_number', TextType::class, ['constraints' => [new NotBlank()]])
                ->add('card_month', ChoiceType::class, ['choices' => $months])
                ->add('card_year', ChoiceType::class, ['choices' => $years])
                ->add('card_cvv', TextType::class, ['constraints' => [new NotBlank()]])
                ;
        } else {
            if ($this->user->getCreditCards()->count() > 0) {
                $builder->add('use_old_credit_card', EntityType::class, [
                    'class' => CreditCard::class,
                    'query_builder' => function (CreditCardRepository $repository) {
                        return $repository->createQueryBuilder('a')
                            ->where('a.user = :user')
                            ->setParameters(['user' => $this->user->getId()])
                            ;
                    },
                    'choice_label' => function ($creditCard) {
                        return '**** **** **** '.$creditCard->getLastFour();
                    },
                    'required' => false,
                    'attr' => ['class' => 'use-old-card'],
                ]);
            }

            $builder
                ->add('card_number', TextType::class)
                ->add('card_month', ChoiceType::class, ['choices' => $months])
                ->add('card_year', ChoiceType::class, ['choices' => $years])
                ->add('card_cvv', TextType::class)
                ->add('card_save', ChoiceType::class, [
                    'required' => true,
                    'choices' => ['Yes' => 1, 'No' => 0],
                ])
                ;
        }
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'checkout';
    }
}
