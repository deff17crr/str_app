<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="default_index")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $products = $this->get('app.product_manager')->getFindAllPaginator($page, 12);

        return $this->render('AppBundle:Default:index.html.twig', [
            'products' => $products,
            'total' => $products->getTotalItemCount(),
        ]);
    }

    /**
     * @Route("/product/{product}", name="default_product_view")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function viewProductAction(Request $request, Product $product)
    {
        return $this->render('@App/Default/viewProduct.html.twig', ['product' => $product]);
    }
}
