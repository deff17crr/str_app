<?php
namespace AppBundle\Controller;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Product;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Form\CheckoutType;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController
 * @package AppBundle\Controller
 * @Route("/cart-item")
 */
class CartItemController extends Controller
{
    const COOKIE_NAME = 'user_token';

    /**
     * @Route("/list", name="cart_item_list", options={"expose"="true"})
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        try {
            $userToken = $request->cookies->get(self::COOKIE_NAME);
            $cartItems = $this->get('app.cart_item_manager')->getArrayItemsForUserToken($userToken);

            return new JsonResponse($cartItems);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * @Route("/add/{product}", name="cart_item_add", options={"expose"="true"})
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function addAction(Product $product, Request $request)
    {
        try {
            $cartItemManager = $this->get('app.cart_item_manager');
            $userToken = $request->cookies->get(self::COOKIE_NAME);
            if (null === $userToken) {
                $userToken = md5(uniqid(rand(), true));

                $response = new JsonResponse();
                $response->headers->setCookie(new Cookie(self::COOKIE_NAME, $userToken, time() + 86400 * 90));
                $response->sendHeaders();
            }

            $cartItemManager->addItemToCart($product, $userToken);
            $cartItems = $cartItemManager->getArrayItemsForUserToken($userToken);

            return new JsonResponse($cartItems);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }


    /**
     * @Route("/remove/{cartItem}", name="cart_item_remove", options={"expose"="true"})
     * @param CartItem $cartItem
     * @param Request  $request
     * @return Response
     */
    public function removeItemAction(CartItem $cartItem, Request $request)
    {
        try {
            $cartItemManager = $this->get('app.cart_item_manager');
            $userToken = $request->cookies->get(self::COOKIE_NAME);
            if ($cartItem->getUserToken() !== $userToken) {
                throw $this->createNotFoundException();
            }

            $cartItemManager->remove($cartItem);

            return new Response('ok');
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }
}
