<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/user")
 * Class AdminUserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="admin_user_index")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $users = $this->get('app.user_manager')->getFindAllPaginator($page);

        return $this->render('AppBundle:Admin/User:index.html.twig', [
            'users' => $users,
            'total' => $users->getTotalItemCount(),
        ]);
    }

    /**
     * @Route("/create", name="admin_user_create")
     * @Route("/edit/{user}", name="admin_user_edit")
     * @param Request $request
     * @param User    $user
     * @return Response
     */
    public function manageAction(Request $request, User $user = null)
    {
        $isNew = null === $user;

        $form = $this->createForm(UserType::class, $user, ['require_password' => $isNew]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setUsername($user->getEmail());
            $this->get('app.user_manager')->save($user);

            $this->get('session')->getFlashBag()->add('success', 'user.success_save');

            return $this->redirectToRoute('admin_user_edit', ['user' => $user->getId()]);
        }

        return $this->render('AppBundle:Admin/User:manage.html.twig', [
            'form' => $form->createView(),
            'isNew' => $isNew,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/delete/{user}", name="admin_user_delete")
     * @param User $user
     * @return Response
     */
    public function deleteAction(User $user)
    {
        $this->get('app.user_manager')->remove($user);

        return $this->redirectToRoute('admin_user_index');
    }
}
