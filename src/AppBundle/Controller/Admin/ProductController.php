<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/product")
 * Class AdminProductController
 * @package AppBundle\Controller
 */
class ProductController extends Controller
{
    /**
     * @Route("/", name="admin_product_index")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $products = $this->get('app.product_manager')->getFindAllPaginator($page);

        return $this->render('AppBundle:Admin/Product:index.html.twig', [
            'products' => $products,
            'total' => $products->getTotalItemCount(),
        ]);
    }

    /**
     * @Route("/create", name="admin_product_create")
     * @Route("/edit/{product}", name="admin_product_edit")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function manageAction(Request $request, Product $product = null)
    {
        $isNew = null === $product;

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Product $product */
            $product = $form->getData();
            $this->get('app.product_manager')->save($product);

            $this->get('session')->getFlashBag()->add('success', 'product.success_save');

            return $this->redirectToRoute('admin_product_edit', ['product' => $product->getId()]);
        }

        return $this->render('AppBundle:Admin/Product:manage.html.twig', [
            'form' => $form->createView(),
            'isNew' => $isNew,
            'product' => $product,
        ]);
    }

    /**
     * @Route("/delete/{product}", name="admin_product_delete")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function deleteAction(Request $request, Product $product)
    {
        $this->get('app.product_manager')->remove($product);

        return $this->redirectToRoute('admin_product_index');
    }
}
