<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/order")
 * Class AdminOrderController
 * @package AppBundle\Controller
 */
class OrderController extends Controller
{
    /**
     * @Route("/", name="admin_order_index")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $orders = $this->get('app.order_manager')->getFindAllPaginator($page);

        return $this->render('AppBundle:Admin/Order:index.html.twig', [
            'orders' => $orders,
            'total' => $orders->getTotalItemCount(),
        ]);
    }

    /**
     * @Route("/view/{order}", name="admin_order_view")
     * @param Order $order
     * @return Response
     */
    public function viewAction(Order $order)
    {
        return $this->render('@App/Admin/Order/view.html.twig', ['order' => $order]);
    }
}
