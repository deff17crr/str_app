<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/transaction")
 * Class AdminTransactionController
 * @package AppBundle\Controller
 */
class TransactionController extends Controller
{
    /**
     * @Route("/", name="admin_transaction_index")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page', 1);
        $transactions = $this->get('app.transaction_manager')->getFindAllPaginator($page);

        return $this->render('AppBundle:Admin/Transaction:index.html.twig', [
            'transactions' => $transactions,
            'total' => $transactions->getTotalItemCount(),
        ]);
    }

    /**
     * @Route("/refund/{transaction}", name="admin_transaction_refund")
     * @param Transaction $transaction
     * @return Response
     */
    public function refundAction(Transaction $transaction)
    {
        if ($transaction->getStatus() !== Transaction::STATUS_APPROVED) {
            throw $this->createNotFoundException();
        }

        // TODO refund

        return $this->redirectToRoute('admin_transaction_index');
    }
}
