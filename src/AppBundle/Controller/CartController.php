<?php
namespace AppBundle\Controller;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Product;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Form\CheckoutType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController
 * @package AppBundle\Controller
 * @Route("/cart")
 */
class CartController extends Controller
{
    const COOKIE_NAME = 'user_token';

    /**
     * @Route("/", name="cart_index", options={"expose"="true"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $cartItemManager = $this->get('app.cart_item_manager');

        $userToken = $request->cookies->get(self::COOKIE_NAME);
        $cartItems = $cartItemManager->getItemsForUserToken($userToken);

        return $this->render('AppBundle:Cart:index.html.twig', [
            'cartItems' => $cartItems,
            'total' => $cartItemManager->countTotalAmount($cartItems),
            'totalItems' => $cartItemManager->countTotalItems($cartItems),
        ]);
    }

    /**
     * @Route("/checkout", name="cart_checkout")
     * @param Request $request
     * @return Response
     */
    public function checkoutAction(Request $request)
    {
        $cartItemManager = $this->get('app.cart_item_manager');
        $em = $this->get('doctrine.orm.default_entity_manager');

        $userToken = $request->cookies->get(self::COOKIE_NAME);
        $cartItems = $cartItemManager->getItemsForUserToken($userToken);
        if (count($cartItems) == 0) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(CheckoutType::class);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $user = $this->getUser();
            $amount = ceil($cartItemManager->countTotalAmount($cartItems));

            $em->beginTransaction();
            try {
                if ($data['use_old_credit_card'] instanceof CreditCard) {
                    $paymentData = $data['use_old_credit_card'];
                } elseif ($data['card_save'] && $user instanceof User) {
                    $paymentData = $this->get('app.credit_card_manager')->saveCard($data, $user);
                } else {
                    $paymentData = $data;
                }

                $transaction = $this->get('app.transaction_manager')->createTransaction($amount, $paymentData, $user);
                $order = $this->get('app.order_manager')->createOrder($user, $transaction, $data);
                $this->get('app.order_item_manager')->createItems($order, $cartItems);
                $this->get('app.cart_item_manager')->clearCart($userToken);
                $em->commit();

                $this->get('session')->getFlashBag()->add('success', sprintf('Successfully charged $%s', $amount));

                return $this->redirectToRoute('default_index');
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('danger', $e->getMessage());
                $em->rollback();
            }
        }

        return $this->render('AppBundle:Cart:checkout.html.twig', [
            'form' => $form->createView(),
            'totalAmount' => $cartItemManager->countTotalAmount($cartItems),
            'totalItems' => $cartItemManager->countTotalItems($cartItems),
        ]);
    }
}
