<?php
namespace AppBundle\Repository;

/**
 * Class OrderRepository
 * @package AppBundle\Repository
 */
class OrderRepository extends AbstractBaseRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindAllQB()
    {
        return $this->createQueryBuilder('a')
            ->select('a, b, c')
            ->leftJoin('a.user', 'b')
            ->leftJoin('a.transaction', 'c')
            ;
    }
}
