<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractBaseRepository
 * @package AppBundle\Repository
 */
abstract class AbstractBaseRepository extends EntityRepository
{
    /**
     * @param mixed $entity
     */
    public function remove($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param mixed $entity
     */
    public function save($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}
