<?php
namespace AppBundle\Repository;

/**
 * Class TransactionRepository
 * @package AppBundle\Repository
 */
class TransactionRepository extends AbstractBaseRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindAllQB()
    {
        return $this->createQueryBuilder('a')
            ->select('a, b')
            ->leftJoin('a.user', 'b')
            ;
    }
}
