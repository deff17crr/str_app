<?php
namespace AppBundle\Repository;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\User;

/**
 * Class CartItemRepository
 * @package AppBundle\Repository
 */
class CartItemRepository extends AbstractBaseRepository
{
    /**
     * @param string $userToken
     * @return CartItem[]
     */
    public function getItemsForUserToken($userToken)
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.product', 'c')
            ->where('a.userToken = :user_token')
            ->setParameters(['user_token' => $userToken])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $userToken
     * @return mixed
     */
    public function clearCart($userToken)
    {
        return $this->createQueryBuilder('a')
            ->delete()
            ->where('a.userToken = :user_token')
            ->setParameters(['user_token' => $userToken])
            ->getQuery()
            ->execute();
    }
}
