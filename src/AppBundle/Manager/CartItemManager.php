<?php
namespace AppBundle\Manager;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Repository\CartItemRepository;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * Class CartItemManager
 * @package AppBundle\Manager
 */
class CartItemManager
{
    /**
     * @var CartItemRepository
     */
    private $repository;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    /**
     * CartItemManager constructor.
     * @param CartItemRepository $repository
     * @param CacheManager       $cacheManager
     * @param UploaderHelper     $uploaderHelper
     */
    public function __construct(CartItemRepository $repository, CacheManager $cacheManager, UploaderHelper $uploaderHelper)
    {
        $this->repository = $repository;
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * @param string $userToken
     * @return CartItem[]
     */
    public function getItemsForUserToken($userToken)
    {
        if (null === $userToken || '' === $userToken) {
            return [];
        }

        return $this->repository->getItemsForUserToken($userToken);
    }

    /**
     * @param string $userToken
     * @return array
     */
    public function getArrayItemsForUserToken($userToken)
    {
        $cartItems = $this->getItemsForUserToken($userToken);

        $data = [];
        foreach ($cartItems as $cartItem) {
            $path = $this->uploaderHelper->asset($cartItem->getProduct(), 'imageFile');
            $thumbPath = $this->cacheManager->getBrowserPath($path, 'product_cart_thumb');
            $data[] = [
                'id' => $cartItem->getId(),
                'quantity' => $cartItem->getQuantity(),
                'product_title' => $cartItem->getProduct()->getTitle(),
                'product_image' => $thumbPath,
                'price' => $cartItem->getProduct()->getPrice(),
            ];
        }

        return $data;
    }

    /**
     * @param CartItem[] $cartItems
     * @return float|integer
     */
    public function countTotalAmount(array $cartItems)
    {
        $totalAmount = 0;
        foreach ($cartItems as $cartItem) {
            $totalAmount += ($cartItem->getProduct()->getPrice() * $cartItem->getQuantity());
        }

        return $totalAmount;
    }

    /**
     * @param CartItem[] $cartItems
     * @return integer
     */
    public function countTotalItems(array $cartItems)
    {
        $totalItems = 0;
        foreach ($cartItems as $cartItem) {
            $totalItems += $cartItem->getQuantity();
        }

        return $totalItems;
    }

    /**
     * @param Product $product
     * @param string  $userToken
     */
    public function addItemToCart(Product $product, $userToken)
    {
        /** @var CartItem $cartItem */
        $cartItem = $this->repository->findOneBy(['product' => $product->getId(), 'userToken' => $userToken]);

        if (null !== $cartItem) {
            $cartItem->setQuantity($cartItem->getQuantity() + 1);
        } else {
            $cartItem = new CartItem();
            $cartItem->setQuantity(1);
            $cartItem->setProduct($product);
            $cartItem->setUserToken($userToken);
        }

        $this->save($cartItem);
    }

    /**
     * @param string $userToken
     */
    public function clearCart($userToken)
    {
        $this->repository->clearCart($userToken);
    }

    /**
     * @param CartItem $cartItem
     */
    public function save(CartItem $cartItem)
    {
        $this->repository->save($cartItem);
    }

    /**
     * @param CartItem $cartItem
     */
    public function remove(CartItem $cartItem)
    {
        $this->repository->remove($cartItem);
    }
}
