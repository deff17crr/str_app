<?php
namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

/**
 * Class UserManager
 * @package AppBundle\Manager
 */
class UserManager
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * UserManager constructor.
     * @param UserRepository $repository
     * @param Paginator      $paginator
     */
    public function __construct(UserRepository $repository, Paginator $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * @param integer $page
     * @param integer $limit
     * @param string  $sortFieldName
     * @param string  $sortFieldDirection
     * @return SlidingPagination
     */
    public function getFindAllPaginator($page, $limit = 10, $sortFieldName = "a.id", $sortFieldDirection = "desc")
    {
        $queryBuilder = $this->repository->createQueryBuilder('a');

        return $this->paginator->paginate($queryBuilder, $page, $limit, [
            'defaultSortFieldName' => $sortFieldName,
            'defaultSortDirection' => $sortFieldDirection,
        ]);
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $this->repository->save($user);
    }

    /**
     * @param User $user
     */
    public function remove(User $user)
    {
        $this->repository->remove($user);
    }
}
