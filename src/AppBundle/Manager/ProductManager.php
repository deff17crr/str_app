<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

/**
 * Class ProductManager
 * @package AppBundle\Manager
 */
class ProductManager
{
    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * ProductManager constructor.
     * @param ProductRepository $repository
     * @param Paginator         $paginator
     */
    public function __construct(ProductRepository $repository, Paginator $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * @param integer $page
     * @param integer $limit
     * @param string  $sortFieldName
     * @param string  $sortFieldDirection
     * @return SlidingPagination
     */
    public function getFindAllPaginator($page, $limit = 10, $sortFieldName = "a.id", $sortFieldDirection = "desc")
    {
        $queryBuilder = $this->repository->createQueryBuilder('a');

        return $this->paginator->paginate($queryBuilder, $page, $limit, [
            'defaultSortFieldName' => $sortFieldName,
            'defaultSortDirection' => $sortFieldDirection,
        ]);
    }

    /**
     * @param Product $product
     */
    public function save(Product $product)
    {
        $this->repository->save($product);
    }

    /**
     * @param Product $product
     */
    public function remove(Product $product)
    {
        $this->repository->remove($product);
    }
}
