<?php
namespace AppBundle\Manager;

use AppBundle\Entity\Order;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Repository\OrderRepository;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

/**
 * Class OrderManager
 * @package AppBundle\Manager
 */
class OrderManager
{
    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * OrderManager constructor.
     * @param OrderRepository $repository
     * @param Paginator       $paginator
     */
    public function __construct(OrderRepository $repository, Paginator $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * @param integer $page
     * @param integer $limit
     * @param string  $sortFieldName
     * @param string  $sortFieldDirection
     * @return SlidingPagination
     */
    public function getFindAllPaginator($page, $limit = 10, $sortFieldName = "a.id", $sortFieldDirection = "desc")
    {
        $queryBuilder = $this->repository->getFindAllQB();

        return $this->paginator->paginate($queryBuilder, $page, $limit, [
            'defaultSortFieldName' => $sortFieldName,
            'defaultSortDirection' => $sortFieldDirection,
        ]);
    }

    /**
     * @param User        $user
     * @param Transaction $transaction
     * @param array       $data
     * @return Order
     */
    public function createOrder(User $user, Transaction $transaction, array $data)
    {
        $order = new Order();
        $order->setUser($user);
        $order->setTransaction($transaction);
        $order->setCity($data['city']);
        $order->setAddress($data['address']);
        $order->setFirstName($data['firstName']);
        $order->setLastName($data['lastName']);

        $this->save($order);

        return $order;
    }

    /**
     * @param Order $order
     */
    public function save(Order $order)
    {
        $this->repository->save($order);
    }

    /**
     * @param Order $order
     */
    public function remove(Order $order)
    {
        $this->repository->remove($order);
    }
}
