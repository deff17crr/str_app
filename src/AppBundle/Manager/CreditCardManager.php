<?php
namespace AppBundle\Manager;

use AppBundle\Entity\CreditCard;
use AppBundle\Entity\User;
use AppBundle\Repository\CreditCardRepository;
use Stripe\Stripe;

/**
 * Class CreditCardManager
 * @package AppBundle\Manager
 */
class CreditCardManager
{
    /**
     * @var CreditCardRepository
     */
    private $repository;

    /**
     * @var
     */
    private $stripeToken;

    /**
     * CreditCardManager constructor.
     * @param CreditCardRepository $repository
     * @param string               $stripeToken
     */
    public function __construct(CreditCardRepository $repository, $stripeToken)
    {
        $this->repository = $repository;
        $this->stripeToken = $stripeToken;
    }

    /**
     * @param array $data
     * @param User  $user
     * @return CreditCard|string
     */
    public function saveCard(array $data, User $user)
    {
        Stripe::setApiKey($this->stripeToken);

        $card = \Stripe\Customer::create(array(
            "email" => $user->getEmail(),
            "card" => array(
                "number" => $data['card_number'],
                "exp_month" => $data['card_month'],
                "exp_year" => $data['card_year'],
                "cvc" => $data['card_cvv'],
            ),
        ));

        $creditCard = new CreditCard();
        $creditCard->setUser($user);
        $creditCard->setToken($card->id);
        $creditCard->setLastFour(substr($data['card_number'], -4));

        $this->save($creditCard);

        return $creditCard;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function save(CreditCard $creditCard)
    {
        $this->repository->save($creditCard);
    }

    /**
     * @param CreditCard $creditCard
     */
    public function remove(CreditCard $creditCard)
    {
        $this->repository->remove($creditCard);
    }
}
