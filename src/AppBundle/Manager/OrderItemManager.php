<?php
namespace AppBundle\Manager;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\Order;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Repository\OrderItemRepository;
use AppBundle\Repository\OrderRepository;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

/**
 * Class OrderItemManager
 * @package AppBundle\Manager
 */
class OrderItemManager
{
    /**
     * @var OrderRepository
     */
    private $repository;

    /**
     * OrderItemManager constructor.
     * @param OrderItemRepository $repository
     */
    public function __construct(OrderItemRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Order            $order
     * @param array|CartItem[] $cartItems
     */
    public function createItems(Order $order, array $cartItems)
    {
        foreach ($cartItems as $cartItem) {
            $orderItem = new OrderItem();
            $orderItem->setProduct($cartItem->getProduct());
            $orderItem->setQuantity($cartItem->getQuantity());
            $orderItem->setOrder($order);

            $this->save($orderItem);
        }
    }

    /**
     * @param OrderItem $orderItem
     */
    public function save(OrderItem $orderItem)
    {
        $this->repository->save($orderItem);
    }

    /**
     * @param OrderItem $orderItem
     */
    public function remove(OrderItem $orderItem)
    {
        $this->repository->remove($orderItem);
    }
}
