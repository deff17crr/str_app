<?php
namespace AppBundle\Manager;

use AppBundle\Entity\CreditCard;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use AppBundle\Repository\TransactionRepository;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Stripe\Stripe;

/**
 * Class TransactionManager
 * @package AppBundle\Manager
 */
class TransactionManager
{
    /**
     * @var TransactionRepository
     */
    private $repository;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * @var string
     */
    private $stripeToken;

    /**
     * TransactionManager constructor.
     * @param TransactionRepository $repository
     * @param Paginator             $paginator
     * @param string                $stripeToken
     */
    public function __construct(TransactionRepository $repository, Paginator $paginator, $stripeToken)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
        $this->stripeToken = $stripeToken;
    }

    /**
     * @param integer $page
     * @param integer $limit
     * @param string  $sortFieldName
     * @param string  $sortFieldDirection
     * @return SlidingPagination
     */
    public function getFindAllPaginator($page, $limit = 10, $sortFieldName = "a.id", $sortFieldDirection = "desc")
    {
        $queryBuilder = $this->repository->getFindAllQB();

        return $this->paginator->paginate($queryBuilder, $page, $limit, [
            'defaultSortFieldName' => $sortFieldName,
            'defaultSortDirection' => $sortFieldDirection,
        ]);
    }

    /**
     * @param integer          $amount
     * @param array|CreditCard $paymentData
     * @param User             $user
     * @return Transaction|string
     */
    public function createTransaction($amount, $paymentData, $user)
    {
        Stripe::setApiKey($this->stripeToken);

        if (is_array($paymentData)) {
            $card = \Stripe\Customer::create(array(
                "email" => $user->getEmail(),
                "card" => array(
                    "number" => $paymentData['card_number'],
                    "exp_month" => $paymentData['card_month'],
                    "exp_year" => $paymentData['card_year'],
                    "cvc" => $paymentData['card_cvv'],
                ),
            ));
            $customer = $card->id;
        } else {
            $customer = $paymentData->getToken();
        }

        $charge = \Stripe\Charge::create(array(
            "amount" => $amount,
            "currency" => "usd",
            "customer" => $customer,
        ));

        $transaction = new Transaction();
        $transaction->setUser($user);
        $transaction->setVendorId($charge->id);
        $transaction->setAmount($amount);
        $transaction->setStatus(Transaction::STATUS_APPROVED);
        $this->save($transaction);

        return $transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function save(Transaction $transaction)
    {
        $this->repository->save($transaction);
    }

    /**
     * @param Transaction $transaction
     */
    public function remove(Transaction $transaction)
    {
        $this->repository->remove($transaction);
    }
}
