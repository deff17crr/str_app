<?php
namespace AppBundle\Menu;

use AppBundle\Entity\User;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Builder
 * @package AppBundle\Menu
 */
class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     * @return \Knp\Menu\ItemInterface
     */
    public function adminMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('labels.products', array('route' => 'admin_product_index'));
        $menu->addChild('labels.users', array('route' => 'admin_user_index'));
        $menu->addChild('labels.transactions', array('route' => 'admin_transaction_index'));
        $menu->addChild('labels.orders', array('route' => 'admin_order_index'));
        $menu->addChild('verb.logout', array('route' => 'fos_user_security_logout'));

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     * @return \Knp\Menu\ItemInterface
     */
    public function frontendMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        /** @var ContainerInterface $container */
        $container = $this->container;
        /** @var User $user */
        $user = $container->get('security.token_storage')->getToken()->getUser();
        $translator = $container->get('translator');

        $menu->addChild('labels.home', array('route' => 'default_index'));

        if ($user instanceof User && $user->hasRole(User::ROLE_ADMIN)) {
            $menu->addChild('labels.admin_panel', ['route' => 'admin_product_index']);
        }

        if ($user instanceof User) {
            $label = sprintf('%s (%s)', $translator->trans('verb.logout'), $user->getEmail());
            $menu->addChild($label, array('route' => 'fos_user_security_logout'));
        } else {
            $menu->addChild('labels.registration', array('route' => 'fos_user_registration_register'));
            $menu->addChild('verb.login', array('route' => 'fos_user_security_login'));
        }

        return $menu;
    }
}
