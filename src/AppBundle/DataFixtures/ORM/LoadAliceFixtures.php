<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Nelmio\Alice\Fixtures;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Console\Helper\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

/**
 * Class LoadAliceFixtures
 */
class LoadAliceFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $stopWatch = new Stopwatch();

        /* remove previously uploaded product images */
        $files = glob($this->container->getParameter('kernel.root_dir').'/../web/uploads/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        /**
         * @param string $message
         */
        $lapCallback = function ($message) use (&$stopWatch) {
            $event = $stopWatch->lap('lap');
            $eventPeriods = $event->getPeriods();
            $lastPeriod = array_pop($eventPeriods);
            echo sprintf(
                '%s => Execution time: %.2f seconds, memory usage: %s.%s',
                $message,
                $lastPeriod->getDuration() / 1000,
                Helper::formatMemory($lastPeriod->getMemory()),
                PHP_EOL
            );
        };

        $stopWatch->start('process');
        $stopWatch->start('lap');
        foreach ($this->getAliceFiles() as $file) {
            try {
                Fixtures::load(
                    $file,
                    $manager,
                    ['providers' => [$this]],
                    []
                );
                $lapCallback($file);
            } catch (\Exception $ex) {
                echo "Exception when loading file $file\n";
                throw $ex;
            }
        }
        $event = $stopWatch->stop('process');
        echo sprintf(
            'Done! Overall execution time: %.2f seconds, memory usage: %s.%s',
            $event->getDuration() / 1000,
            Helper::formatMemory($event->getMemory()),
            PHP_EOL
        );
    }

    /**
     * @param Object $object
     * @param string $collectionField
     * @return mixed
     */
    public function randomCollectionElement($object, $collectionField)
    {
        $this->manager->refresh($object);

        $getter = 'get'.ucfirst($collectionField);
        if (!method_exists($object, $getter) || !is_callable([$object, $getter])) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Unable to load collection "%s" from the object "%s".',
                    $collectionField,
                    is_object($object) ? get_class($object) : gettype($object)
                )
            );
        }

        $collection = $object->$getter();
        if ($collection instanceof Collection) {
            $collection = $collection->toArray();
        }

        return Factory::create()->randomElement($collection);
    }

    /**
     * @return UploadedFile|void
     * @throws \Exception
     */
    public function uploadProductImage()
    {
        $filePath = __DIR__.DIRECTORY_SEPARATOR.'alice_configs'.DIRECTORY_SEPARATOR.'product_images'.DIRECTORY_SEPARATOR.rand(1, 15).'.jpg';

        $path = sprintf('/tmp/%s', uniqid());
        $copy = copy($filePath, $path);
        if (!$copy) {
            throw new \Exception('Copy failed');
        }

        $mimeType = MimeTypeGuesser::getInstance()->guess($path);
        $size = filesize($path);
        $file = new UploadedFile($path, $filePath, $mimeType, $size, null, true);

        return $file;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 0;
    }

    /**
     * @return array|Finder
     */
    protected function getAliceFiles()
    {
        return Finder::create()
            ->ignoreVCS(true)
            ->files()
            ->name('*.yml')
            ->sortByName()
            ->in(__DIR__.DIRECTORY_SEPARATOR.'alice_configs');
    }
}
